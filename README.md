# DCAM Layer for Keras

The DCAM.py file contains a class which implements a keras layer applying the Dual Chamber Absorption Model. The parameters of the layer are automatically optimised during training alongside the rest of the model parameters to minimise the loss.

![](images/model.jpg)

## Masking
The layer constructor requires the sparse_features parameter, an iterable containing the columns the layers should be applied to. Columns not specified are ignored and passed through. 

## Custom Equations

The equation of this layer can be easily modified by editing lambda function inside the dexp method.